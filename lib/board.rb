# In your Board class, you should have a grid instance variable to keep
# track of the board tiles. You should also have the following methods:
#
# place_mark: which takes a position such as [0, 0] and a mark such as
              # :X as arguments. It should throw an error if the position
              # isn't empty.
# empty?: which takes a position as an argument
# winner: which should return a mark
# over?: which should return true or false


class Board
  attr_reader :grid, :marks

  def self.blank_grid
    [[nil, nil, nil],[nil, nil, nil],[nil, nil, nil]]
  end

  def initialize(grid = Board.blank_grid)
    @grid = grid
    @marks = [:X, :O]
  end

  def [](row, col)
    grid[row][col]
  end

  def []=(row, col, value)
    grid[row][col] = value
  end

  def over?
    grid.flatten.none? { |pos| pos.nil? } || winner
  end

  def empty?(pos)
    self[*pos].nil?
  end

  def place_mark(pos, mark)
    self[*pos] = mark
  end

  def winner
    (grid + cols + diagonals).each do |triple|
      return :X if triple == [:X, :X, :X]
      return :O if triple == [:O, :O, :O]
    end

    nil
  end

  def cols
    cols = [[], [], []]
    grid.each do |row|
      row.each_with_index do |mark, col_idx|
        cols[col_idx] << mark
      end
    end

    cols
  end

  def diagonals
    down_diag = [[0, 0], [1, 1], [2, 2]]
    up_diag = [[0, 2], [1, 1], [2, 0]]

    [down_diag, up_diag].map do |diag|
      diag.map { |row, col| grid[row][col] }
    end
  end
end
