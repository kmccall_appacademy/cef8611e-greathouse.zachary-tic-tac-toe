# display:  should store the board it's passed as an instance variable,
#           so that get_move has access to it
# get_move: should return a winning move if one is available, and
#           otherwise move randomly


class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    acceptable_moves = []

    #iteratet through rows and columns recording all the spots left to make a move
    (0..2).each do |row|
      (0..2).each do |col|
        acceptable_moves << [row, col] if board[row, col].nil?
      end
    end

    #if one of the acceptable moves if a winning move, choose it
    acceptable_moves.each do |move|
      return move if wins?(move)
    end

    #select random move from acceptable_moves
    acceptable_moves.sample
  end

  def wins?(move)
    board[*move] = mark
    if board.winner == mark
      board[*move] = nil
      true
    else
      board[*move] = nil
      false
    end
  end

  def display(board)
    @board = board
  end
end
