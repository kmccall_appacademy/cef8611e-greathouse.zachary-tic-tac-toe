# display:  should print the board out to the console
# get_move: should allow the player to enter a move of
#           the form '0, 0', and return it as a position
#           of the form [0, 0]



class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where? (ex. 1, 1)"
    gets.chomp.split(", ").map(&:to_i)
  end

  def display(board)
    row0 = "0 |"
    (0..2).each do |col|
      if board.empty?([0, col])
        row0 << "   |"
      else
        row0 << " " + board[0, col].to_s + " |"
      end
    end
    row1 = "1 |"
    (0..2).each do |col|
      if board.empty?([1, col])
        row1 << "   |"
      else
        row1 << " " + board[1, col].to_s + " |"
      end
    end
    row2 = "2 |"
    (0..2).each do |col|
      if board.empty?([2, col])
        row2 << "   |"
      else
        row2 << " " + board[2, col].to_s + " |"
      end
    end

    puts "    0   1   2  "
    puts "  |-----------|"
    puts row0
    puts "  |-----------|"
    puts row1
    puts "  |-----------|"
    puts row2
    puts "  |-----------|"
  end
end
